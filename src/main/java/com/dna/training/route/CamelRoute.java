package com.dna.training.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dna.training.CamelProcessor;
import com.dna.training.exception.MyException;

@Component
public class CamelRoute extends RouteBuilder {

	@Autowired
	CamelProcessor camelProcessor;
	
	@Override
	public void configure() throws Exception {
		
		onException(MyException.class).continued(true);
		
		
		from("timer://streamTimer?delay=3000")
		.setBody(simple("Hi I am here")).throwException(new MyException("In Exception block"))
		.to("stream:out");
		
		//Rest Configuration for consumer. host and port is of the producer
		//which hosts this Endpoint.
		
		  restConfiguration().component("servlet").host("localhost")
		  .port("8090") .bindingMode(RestBindingMode.json);
		  
		  from("rest:get:helloAll")
		  .transform().constant("Hello from Camel");
		  
		  //Active MQ producer
		  from("file://<SRC_FILE_LOCALTION>?delete=true")
		  .process(camelProcessor)
		  .split().tokenize("\n")
		  	.choice()
			  	.when()
			  		.simple("${body} contains 'first'")
			  		.to("jms:queue:camelmq-1")
		  		.when()
		  			.simple("${body} contains 'second'")
		  			.to("jms:queue:camelmq-2")
		  		.otherwise()
		  			.to("jms:queue:camelmq-3")
		  	.endChoice();
		  //.to("stream:out");
		  
		  //Active MQ consumer		  
		  from("jms:queue:camelmq-1").process(exc->{
			  String queueMessage = exc.getMessage().getBody().toString().trim() + " mine";
			  exc.getMessage().setBody(queueMessage);
		  }).to("file://<DEST_FILE_LOCALTION>?noop=true")
		  .end();
	}

}
