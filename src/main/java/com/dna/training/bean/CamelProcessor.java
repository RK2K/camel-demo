/**
 * 
 */
package com.dna.training.bean;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author kuzhali.r
 *
 */
@Component
public class CamelProcessor implements Processor {

	Logger LOGGER = LoggerFactory.getLogger(CamelProcessor.class);
	@Override
	public void process(Exchange exchange) throws Exception {
		LOGGER.info("Inside processor method");
	}

}
