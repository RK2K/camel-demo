/**
 * 
 */
package com.dna.training.bean;

import org.springframework.stereotype.Service;

/**
 * @author kuzhali.r
 *
 */
@Service
public class HelloBean {

	public String hello() {
		return "Hello there";
	}
}
