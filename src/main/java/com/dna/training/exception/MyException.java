/**
 * 
 */
package com.dna.training.exception;

/**
 * @author kuzhali.r
 *
 */
public class MyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2053634294574653666L;

	/**
	 * @param message
	 */
	public MyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	
}
